public class Delos extends Actor
{
    GreenfootSound sound = new GreenfootSound("wind.wav");
    public void act()
    {
       checkKeypress(); 
       lookforKillerwhale();
       TurnAtEdge();
       
    }
 
    public void checkKeypress()   
    {    
        if (Greenfoot.isKeyDown("a"))
        {
            turn(-5);
        }
        
        if (Greenfoot.isKeyDown("d"))
        {
            turn(5);
        }
        
        if (Greenfoot.isKeyDown("space"))
        {
            move(6);
            sound.play();
        }   
        if (Greenfoot.isKeyDown("w"))
        {
            move(3);
            sound.stop();
        }
        if (Greenfoot.isKeyDown("s"))
        {
            move(-3);
        }
        
    }
    public void lookforKillerwhale()
    {
        
        if (isTouching(Killerwhale.class))
         {   
             removeTouching(Killerwhale.class);
        }
    }
    
    public void TurnAtEdge()
    {   
        if (isAtEdge())
        {        
            turn(-13);
        }
}  
}



public class Killerwhale extends Actor
{
    
    public void act() 
    {
        randomTurn();
        move(3);
        TurnAtEdge();
    }    

    public void randomTurn()
    {
        if  (Greenfoot.getRandomNumber(100) > 90)
        {
           turn(Greenfoot.getRandomNumber(90)-45); 
        }
    }
    public void TurnAtEdge()
    {   
        if (isAtEdge())
        turn(-12);
    }
}



public class GameOver extends Actor
{
        public GameOver()
    {
        setImage(new GreenfootImage("Don't hit lighthouses!", 48, Color.YELLOW, Color.BLUE));
    }
}



public class Lighthouse extends Actor
{   
    public void act()
    {
        lookforDelos();
    }
    
    public void lookforDelos()
    {
      if (isTouching(Delos.class) )
      {
            removeTouching(Delos.class);
            World myWorld = getWorld();
            GameOver gameover = new GameOver();
            myWorld.addObject(gameover, myWorld.getWidth()/2, myWorld.getHeight()/2);
            Greenfoot.stop();
      }
    }
}




